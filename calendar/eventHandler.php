<?php

//incluimos archivo dbConfig

require_once 'dbConfig.php';

//Recuperamos json del bodyclear

$jsonStr = file_get_contents('php://input');
$jsonObj = json_decode($jsonStr);

if($jsonObj->request_type == 'addEvent'){
    $start = $jsonObj->start;
    $end = $jsonObj->end;

    $event_data = $jsonObj->event_data;
    $eventTitle = !empty($event_data[0])?$event_data[0]:'';
    $eventDesc = !empty($event_data[1])?$event_data[1]:'';
    $eventDesc2 = !empty($event_data[2])?$event_data[2]:'';
    $eventBackgroundColor = !empty($event_data[3])?$event_data[3]:'';

    if(!empty($eventTitle)){
        //Consulta para insertar datos
        $sqlQ = "INSERT INTO eventos (title,descripcion,descripcion2,backgroundColor,start,end) VALUES (?,?,?,?,?,?)";
        $stmt = $db->prepare($sqlQ);
        $stmt->bind_param("ssssss", $eventTitle, $eventDesc, $eventDesc2, $eventBackgroundColor, $start, $end);
        $insert = $stmt->execute();

        if($insert){
            $output = [
                'status' => 1
            ];
            echo json_encode($output);
        }else{
            echo json_encode(['error' => 'Event Add request failed!']);

        }
    }
}elseif($jsonObj->request_type == 'editEvent') {
    $start = $jsonObj->start;
    $end = $jsonObj->end;
    $event_id = $jsonObj->event_id;
    
    $event_data = $jsonObj->event_data;
    $eventTitle = !empty($event_data[0])?$event_data[0]:'';
    $eventDesc = !empty($event_data[1])?$event_data[1]:'';
    $eventDesc2 = !empty($event_data[2])?$event_data[2]:'';
    $eventBackgroundColor = !empty($event_data[3])?$event_data[3]:'';

    if(!empty($eventTitle)){
        //Actualizar en base de datos
        $sqlQ = "UPDATE eventos SET title=?,descripcion=?,descripcion2=?,backgroundColor=?,start=?,end=? WHERE id=?";
        $stmt = $db->prepare($sqlQ);
        $stmt->bind_param("ssssssi", $eventTitle, $eventDesc, $eventDesc2, $eventBackgroundColor, $start, $end, $event_id);
        $update = $stmt->execute(); //6"s"(por variables)+i(event-id)

        if ($update) {
            $output = [
                'status' => 1
            ];
            echo json_encode($output);
        } else {
            echo json_encode(['error' => 'La actualización del evento falló!']);
        }
    }

    }elseif($jsonObj->request_type == 'deleteEvent'){
        $id = $jsonObj->event_id;

        //BORRAR EVENTO EN BASE DATOS
        $sql = "DELETE FROM eventos WHERE id=$id";
        $delete = $db->query($sql);

        if($delete){
            $output = [
                'status' => 1
            ];
            echo json_encode($output); 
        }else{
            echo json_encode(['error' => 'Event Add request failed!']);
        }
    };


    






?>